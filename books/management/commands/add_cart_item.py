from django.core.management.base import BaseCommand, CommandError
from books.models import Book, Cart, CartItem
from random import choices

class Command(BaseCommand):
    def add_cart_item(self):
        cart = list(Cart.objects.all())
        books = list(Book.objects.all())
        for _ in cart:
            cart_items = choices(books,k=3)
            unique_books = set(cart_items)
            book_count = {book:cart_items.count(book) for book in unique_books}
            for book in book_count:
                # print(f'cart={_},book={book},qauntity={book_count[book]}')
                CartItem.objects.create(cart=_,book=book, quantity=book_count[book])

    def handle(self, *args, **options):
        self.add_cart_item()