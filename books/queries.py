from django.db.models import Count, Max, Sum
from books.models import *
def get_book(id):
    book = Book.objects.get(id=id)
    return book

def get_books(id_list):
    books = Book.objects.filter(id__in=(id_list))
    return books

def book_of_specific_category(category):
    category_books = [bookcategory.book for bookcategory in BookCategory.objects.select_related('book').filter(category__name=category)]
    return category_books

def count_category(category):
    book_category_count = BookCategory.objects.filter(category__name=category).count()
    return book_category_count

def max_book_author():
    # # mostBookAuthor.objects.values('author__id').annotate(Count('book_id')).order_by('-book_id__count')[0]["book_id__count"]
    return Author.objects.get(id=BookAuthor.objects.values('author__id').annotate(Count('book_id')).order_by('-book_id__count')[0]["author__id"]).name

def total_book_price():
    total = Book.objects.all().aggregate(Sum('price'))['price__sum']
    return total

def max_priced_book():
    maximum_priced_book = Book.objects.filter(price=Book.objects.all().aggregate(Max('price'))["price__max"])
    return maximum_priced_book

def book_priced_gt_30():
    book_price_gt_30 = Book.objects.filter(price__gt=30).order_by('-price')
    return book_price_gt_30

def delete_book(id):
    return Book.objects.get(id=id).delete()

def Fetch_cart_items_and_corresponding_books(id):
    # CartItem.objects.select_related('book').filter(cart__id=1)
    return [cart_item.book for cart_item in CartItem.objects.select_related('book').filter(cart__id=id)]