from django.core.management.base import BaseCommand, CommandError
from books.models import Book
from random import randint

class Command(BaseCommand):
    def add_books(self):
        for _ in range(1,41): 
            Book.objects.create(title="b"+str(_), price=randint(1,40) )

    def handle(self, *args, **options):
        self.add_books()