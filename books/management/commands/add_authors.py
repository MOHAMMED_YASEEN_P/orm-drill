from django.core.management.base import BaseCommand, CommandError
from books.models import Author
from random import randint

class Command(BaseCommand):
    def add_authors(self):
        for _ in range(1,10): 
            Author.objects.create(name="Author"+str(_))

    def handle(self, *args, **options):
        self.add_authors()