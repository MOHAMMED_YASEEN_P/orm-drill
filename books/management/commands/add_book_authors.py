from django.core.management.base import BaseCommand, CommandError
from books.models import Author, Book, BookAuthor
from random import sample

class Command(BaseCommand):
    def add_book_authors(self):
        books = list(Book.objects.all())
        authors = list(Author.objects.all())
        for book in books:
            book_authors = sample(authors, 2)
            for author in book_authors:
                BookAuthor.objects.create(book=book, author=author)

    def handle(self, *args, **options):
        self.add_book_authors()