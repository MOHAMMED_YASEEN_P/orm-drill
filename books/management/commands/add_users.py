from django.core.management.base import BaseCommand, CommandError
from books.models import User

class Command(BaseCommand):
    def add_users(self):
        for _ in range(1,11): 
            User.objects.create(name="u"+str(_))

    def handle(self, *args, **options):
        self.add_users()