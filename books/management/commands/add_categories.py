from django.core.management.base import BaseCommand, CommandError
from books.models import Category

class Command(BaseCommand):
    def add_categories(self):
        for _ in range(1,11): 
            Category.objects.create(name="c"+str(_))

    def handle(self, *args, **options):
        self.add_categories()