from django.core.management.base import BaseCommand, CommandError
from books.models import Cart, User
from random import choice

class Command(BaseCommand):
    def add_cart(self):
        users = list(User.objects.all())
        for _ in range(1,21): 
            Cart.objects.create(user=choice(users))

    def handle(self, *args, **options):
        self.add_cart()